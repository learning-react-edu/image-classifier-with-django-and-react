import { useEffect, useState } from "react";
import axios from "axios";
import { ImageListItem } from "./ImageListItem";
import { Button, Spinner } from "react-bootstrap";

export const ImageList = () => {
    const [ isLoading, setIsLoading ] = useState(true);
    const [ isLoadingMore, setIsLoadingMore ] = useState(false);
    const [ images, setImages ] = useState([]);
    const [ visible, setVisible ] = useState(2);

    useEffect(() => {
        const getImages = async () => {
            const response = await axios.get('http://127.0.0.1:8000/api/images/', {
                headers: {
                    'accept': 'application/json',
                },
            });
            setImages(response.data);
        };
        getImages();
        setIsLoading(false);
    }, []);

    const loadMore = () => {
        setIsLoadingMore(true);
        setVisible(visible + 2)
        setIsLoadingMore(false);
    }

    return (
        <>
            {isLoading
                ? <Spinner animation="border" role="status"/>
                : <>
                    {images.length === 0 &&
                    <h3>No images classified</h3>}
                    {images.slice(0, visible).map(image => (
                            <ImageListItem
                                key={image.id}
                                picture={image.picture}
                                name={image.classified}/>))}
                    {isLoadingMore &&
                    <Spinner animation="border" role="status"/>}
                    <br/>
                    {images.length > visible
                        ? <Button
                            variant='primary'
                            size='lg'
                            className='mb-3'
                            onClick={loadMore}>Load more</Button>
                        : images.length > 0 &&
                            <h3 className='mb-3'>No more images to load</h3>}
                </>}
        </>
    );
}
