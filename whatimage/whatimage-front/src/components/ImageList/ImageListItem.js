import { Card} from "react-bootstrap";

export const ImageListItem = ({ picture, name }) => {
    return (
        <Card style={{ width: '30rem'}} className='mx-auto mb-2'>
            <Card.Img variant='top' src={picture}/>
            <Card.Body>
                <Card.Title>Classified as: {name}</Card.Title>
            </Card.Body>
        </Card>
    );
};
