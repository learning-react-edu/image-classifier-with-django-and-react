import { Nav, Navbar } from "react-bootstrap";

export const Navigation = () => {
    return (
        <>
            <Navbar variant='dark' bg='dark'>
                <Navbar.Brand href='#'>WhatImage</Navbar.Brand>
                <Nav classname='mr-auto'>
                    <Nav.Link href='/'>Home</Nav.Link>
                    <Nav.Link href='/images'>Image History</Nav.Link>
                </Nav>
            </Navbar>
        </>
    );
};
