import { useCallback, useState } from "react";
import { Alert, Button, Image, Spinner } from "react-bootstrap";
import { useDropzone } from 'react-dropzone';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faImage } from '@fortawesome/fontawesome-free-regular';
import axios from "axios";
import './Classifier.css';

export const Classifier = () => {
    const [ isLoading, setIsLoading ] = useState(false);
    const [ isLoadingToServer, setIsLoadingToServer ] = useState(false);
    const [ files, setFiles ] = useState([]);
    const [ recentImage, setRecentImage ] = useState(null);
    const onDrop = useCallback((acceptedFiles) => {
        setIsLoading(true);
        setFiles([]);
        setRecentImage(null);
        loadImage(acceptedFiles);
    }, []);
    const {
        getRootProps,
        getInputProps,
        isDragActive,
    } = useDropzone({
        accept: 'image/png, image/jpeg',
        onDrop,
    });

    const loadImage = (files) => {
        setTimeout(() => {
            setFiles(files);
            setIsLoading(false);
        }, 1000);
    };

    const sendImage = async () => {
        setFiles([]);
        setIsLoadingToServer(true);
        const formData = new FormData();
        if (files.length > 0) {
            formData.append('picture', files[0], files[0].name);
            try {
                const response = await axios.post('http://127.0.0.1:8000/api/images/', formData, {
                    headers: {
                        'accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                    },
                });
                setRecentImage(response.data);
            } catch (e) {
                console.error('Failed to send image to the server:', e);
            }
            setIsLoadingToServer(false);
        }
    };

    return (
        <section className="container">
            <div {...getRootProps({ className: 'dropzone back' })}>
                <input {...getInputProps()} />
                <FontAwesomeIcon icon={faImage} className="mb-2 text-muted" style={{ fontSize: 100 }}/>
                <p className="text-muted">{isDragActive ? "Drop some images" : "Drag 'n' drop some files here, or click to select files"}</p>
            </div>
            <aside>
                <ul>{files.map(file => (
                    <li key={file.path}>
                        {file.path} - {file.size} bytes
                    </li>
                ))}
                </ul>
            </aside>
            {files.length > 0 &&
            <Button
                variant='info'
                size="lg"
                className='mt-3'
                onClick={sendImage}
            >Select Image</Button>}
            {(isLoading || isLoadingToServer) &&
            <Spinner animation="border" role="status"/>}
            {recentImage &&
            <>
                <Alert variant='primary'>
                    {recentImage.classified}
                </Alert>
                <Image
                    className='justify-content-center'
                    src={recentImage.picture}
                    height='200'
                    rounded/>
            </>}
        </section>
    );
};
