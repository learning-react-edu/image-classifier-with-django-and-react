import './App.css';
import fontawesome from '@fortawesome/fontawesome';
import { faImage } from '@fortawesome/fontawesome-free-regular';
import { BrowserRouter, Route } from "react-router-dom";
import { Classifier } from "./components/Classifier/Classifier";
import { ImageList } from "./components/ImageList/ImageList";
import { Navigation } from "./components/Navigation/Navigation";

fontawesome.library.add(faImage);

function App() {
    return (
        <BrowserRouter>
            <Navigation />
            <div className='App'>
                <Route exact path='/' component={Classifier} />
                <Route path='/images' component={ImageList} />
            </div>
        </BrowserRouter>
    );
}

export default App;
