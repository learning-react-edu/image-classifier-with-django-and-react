import './App.css';
import { useState } from "react";
import { Button, Card, Container } from "react-bootstrap";
import { PostForm } from "./components/PostForm/PostForm";
import { PostList } from "./components/PostList/PostList";
import cuid from "cuid";
import { StarWars } from "./components/StarWars/StarWars";

const fakeData = [
    {
        id: 1,
        text: 'Hello world',
    }, {
        id: 2,
        text: 'Hello again',
    }, {
        id: 3,
        text: 'Bye bye',
    },
];

function App() {
    const [ textValue, setTextValue ] = useState('Hello world from the state');
    const [ posts, setPosts ] = useState(fakeData);
    const [ showPosts, setShowPosts ] = useState(false);

    const togglePostsHandler = () => {
        setShowPosts(!showPosts);
    }

    const addPost = (text) => {
        const newPost = {
            id: cuid(),
            text,
        };
        setPosts([...posts, newPost]);
    }

    const deletePost = (id) => {
        console.log(id);
        setPosts(posts.filter(post => post.id !== id));
    }

    return (
        <div className="App">
            <Container>
                <PostForm addPost={addPost}/>
                <hr/>
                <PostList
                    posts={posts}
                    status={showPosts}
                    deletePost={deletePost}
                    togglePosts={togglePostsHandler} />
                <hr/>
                <StarWars />
            </Container>

            <br/>
            <br/>
            <Container>
                <hr/>
                <h1>{textValue}</h1>
                {textValue
                    ? <Button variant='primary' size='lg' onClick={() => setTextValue(null)}>Test button</Button>
                    : <Button variant='danger' size='lg' onClick={() => setTextValue('Hello again!')}>Test button 2</Button>
                }

                {textValue &&
                <Card>Hi there</Card>
                }
            </Container>
        </div>
    );
}

export default App;
