import { Button } from "react-bootstrap";

export const Post = ({ post, deletePost }) => {
    return (
        <>
            <h3>{post.text}</h3>
            <Button variant='danger' onClick={() => deletePost(post.id)}>Delete</Button>
        </>
    );
};
