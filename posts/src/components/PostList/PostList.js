import { Post } from "./Post";
import { Button } from "react-bootstrap";

export const PostList = ({ posts, status, togglePosts, deletePost }) => {
    return (
        <>
            <Button onClick={togglePosts}>{status ? 'Hide' : 'Show'} posts</Button>
            {status && posts.map(post => (
                <Post post={post} key={post.id} deletePost={deletePost} />
            ))}
        </>
    );
};
