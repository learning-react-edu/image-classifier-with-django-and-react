import { useEffect, useState } from "react";

const API_URL = 'https://swapi.dev/api/people/';

export const StarWars = () => {
    const [ isLoading, setIsLoading ] = useState(true);
    const [ people, setPeople ] = useState([]);

    useEffect(() => {
        const loadPeople = async () => {
            const response = await fetch(API_URL);
            const peopleJson = await response.json();
            setPeople(peopleJson.results);
            setIsLoading(false);
        };
        loadPeople();
    }, []);

    return (
        <ul>
            {isLoading
                ? null
                : people.map(person => (
                    <li key={person.name}>{person.name}</li>
                ))}
        </ul>
    );
};
