import { Button, Form } from "react-bootstrap";
import { useState } from "react";

export const PostForm = ({ addPost }) => {
    const [ postInputValue, setPostInputValue ] = useState('');

    const createPostHandler = () => {
        if (postInputValue.length >= 3) {
            addPost(postInputValue);
            setPostInputValue('');
        }
    }

    return (
        <Form>
            <h3>Write a post</h3>
            <input type="text"
                   className="form-control"
                   value={postInputValue}
                   onChange={e => setPostInputValue(e.target.value)}/>
            <Button variant='info' className='mt-2' onClick={createPostHandler}>Submit</Button>
        </Form>
    );
};
